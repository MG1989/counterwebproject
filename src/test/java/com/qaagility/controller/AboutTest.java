package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest
{
	@Test
	public void testDesc()
	{
		String result = new About().desc();
		String testString = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
		assertEquals("desc", testString, result);	
	}
}
