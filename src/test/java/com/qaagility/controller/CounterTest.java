package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CounterTest
{
	@Test
	public void testDivisionOne()
	{
		int result = new Counter().division(1, 0);
		assertEquals("division", Integer.MAX_VALUE, result);	
	}
	
	@Test
	public void testDivisionTwo()
	{
		int result = new Counter().division(6, 3);
		assertEquals("division", 2, result);
	}
}
