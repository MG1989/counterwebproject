package com.qaagility.controller;

public class Counter {

    public int division(int firstVar, int secondVar) {
        if (secondVar == 0)
	{
            return Integer.MAX_VALUE;
	}
        else
	{
            return firstVar / secondVar;
	}
    }

}
